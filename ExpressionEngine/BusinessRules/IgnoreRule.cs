﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{


    /// <summary>
    /// Validates that the Source of this event is in range of the valid Source to be processed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class IgnoreRule<T> : CompositeRule<T>
    {
        public IgnoreRule(List<T> source) : base(source)
        {
        }

        //Businessrule
        //Return true if item has flag BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent))
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            switch (item.Status)
            {
                case ActivityStatus.BANKREGNR_IKKE_EIKABANK:
                    return true;
                case ActivityStatus.FIKTIVID_IKKE_I_KERNE:
                    return true;
            }
            return false;
        }
    }
}
