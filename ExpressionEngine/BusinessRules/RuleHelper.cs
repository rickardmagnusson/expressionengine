﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace ExpressionEngine.CRM
{
    /// <summary>
    /// Contains logic to generate comparsion rules
    /// </summary>
    public static class RuleHelper
    {
        private static readonly string BREEG = "809020003";
        private static readonly string DSF = "809020003";
        private static readonly string MIGRATE = "809020004";
        private static readonly string ARCHIVE = "809020000";
        private static readonly string STATUSREASONACTIVE = "1";
        private static readonly string STATUS_REASON_INACTIVE = "2";
        private static readonly string INVALID_PUBLICID_FIELD = "cap_invalidpublicid";
        private static readonly string SMS_FIELD = "Contact_Faxes";


        #region Loaders

        /// <summary>
        /// Load contact from cache.
        /// </summary>
        /// <param name="ssn"></param>
        /// <returns>A contact from if the ssn is found</returns>
        public static List<CRMContact> CRMContacts(string ssn)
        {
            var cache = CRMCache.Instance();
            return cache.Contacts.Where(c=> c.governmentid==ssn).ToList(); 
        }


        /// <summary>
        /// Load all BusinessUnits.
        /// </summary>
        /// <returns>List of active BusinessUnits</returns>
        public static List<BusinessUnit> BusinessUnits()
        {
            var cache = CRMCache.Instance();
            return cache.BusinessUnits;
        }


        /// <summary>
        /// Load all BusinessUnits.
        /// </summary>
        /// <returns>List of active BusinessUnits</returns>
        public static List<SMS> GetSMS()
        {
            var cache = CRMCache.Instance();
            return cache.SMS;
        }


        /// <summary>
        /// Load customers into cache fir performance
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of CrmContacts</returns>
        public static List<CRMContact> LoadContacts(this List<KundeRegisterEvent> items)
        {
            var customers = new List<CRMContact>();

            foreach (var item in items)
            {
                var customer = new CRMQuery()
                    .GetBySSN<CRMContact>(item.SSN);

                foreach (var c in customer) {
                    var field = new CRMQuery().SMS<SMS>(c.governmentid);
                    if(field!=null)
                        SMS.Add(new SMS{governmentid=c.governmentid, scheduledend=field[0].scheduledend});
                    customers.Add(c);
                }
            }
            return customers;
        }

        private static  List<SMS> SMS = new List<SMS>();
        /// <summary>
        /// Load customers into cache fir performance
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of CrmContacts</returns>
        public static List<SMS> LoadSMS(this List<KundeRegisterEvent> items)
        {
            return SMS;
        }
        


        /// <summary>
        /// Load all BusinessUnits into cache for performance.
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> of </returns>
        public static List<BusinessUnit> LoadBusinessUnits(this List<KundeRegisterEvent> items)
        {
            var cache = CRMCache.Instance();
            var queryAllBanks = new CRMQuery().BusinessUnits<BusinessUnit>();
            var bankList = new List<BusinessUnit>();

            queryAllBanks.ForEach(b => {
                if (b.divisionname != null)
                    bankList.Add(new BusinessUnit { businessunitid = b.businessunitid, divisionname = b.divisionname });
            });

            return bankList;
        }


        #endregion



        /// <summary>
        /// Get all BusinessUnits
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> of active BusinessUnits</returns>
        public static List<string> GetBusinessUnits(this List<KundeRegisterEvent> items)
        {
            var queryAllBanks = BusinessUnits();
            var bankList = new List<string>();

            queryAllBanks.ForEach(b => {
                if (b.divisionname != null)
                    bankList.Add(b.divisionname);
            });

            return bankList;
        }


        /// <summary>
        /// Get all contacts where statcode = (Active) or (Inactive)
        /// </summary>
        /// <param name="items">List<KunderRegister> items to check</param>
        /// <returns>List of active SNN</returns>
        public static List<string> GetSSN(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                    ssnList.Add(item.SSN);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Get all contacts where statcode = 0 (Active)
        /// </summary>
        /// <param name="items">List<KunderRegister> items to check</param>
        /// <returns>List of active SNN</returns>
        public static List<string> GetActiveSSN(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                    if (Convert.ToInt32(i.statecode) == 0) {
                        ssnList.Add(item.SSN);
                    }
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Applies to #1
        /// Creates a list of ActivityStatus as strings
        /// </summary>
        /// <returns>A list of ActivityStatus enum as strings</returns>
        public static List<string> ActivityStatusFields()
        {
            var list = new List<string>();
            var activityStatus = new List<ActivityStatus> { ActivityStatus.AKTIV, ActivityStatus.AKTIV_RELASJON, ActivityStatus.AVVENTER_SJEKK };

            foreach (ActivityStatus pa in activityStatus)
                list.Add(pa.ToString());
            return list;
        }


        /// <summary>
        /// Check to see if contact has other sources
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> SSN</returns>
        public static List<string> HasOtherSources(this List<KundeRegisterEvent> items)
        {
            //cap_sourceid(Kerne): BU: Any bank
            //cap_tradexsourceid(Tradex) : BU: Kapitalforvaltning
            //cap_nicesourceid(NICE EFS): BU Eika Skadeforsikring
            //cap_nice2sourceid(NICE EFP): BU: Eika Personforsikring
            //cap_banqsoftsourceid(BanQsoft) BU: Eika Kredittbank

            //By priority
            //  Kerne
            //  - NICE
            //  - BanQsoft
            //  - Tradex

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                var customers = CRMContacts(item.SSN);
                customers.ForEach(i => 
                {
                    if (!string.IsNullOrEmpty(i.cap_sourceid) ||
                        !string.IsNullOrEmpty(i.cap_tradexsourceid) || 
                        !string.IsNullOrEmpty(i.cap_nicesourceid) || 
                        !string.IsNullOrEmpty(i.cap_nice2sourceid) || 
                        !string.IsNullOrEmpty(i.cap_banqsoftsourceid) || 
                        !string.IsNullOrEmpty(i.cap_tradexsourceid))
                    {
                        if (!ssnList.Contains(i.governmentid)) //Distinct
                        { 
                            Console.Write("Customer " + i.fullname);
                            Console.Write(" Added: " + i.governmentid);
                            Console.WriteLine("");
                            ssnList.Add(item.SSN);
                        }
                    }
                });
            }
    
            return ssnList;
        }


        /// <summary>
        /// Check of recent activity for a contact
        /// </summary>
        /// <param name="items"></param>
        /// <returns>List<string> SSN</returns>
        public static List<string> RecentActivities(this List<KundeRegisterEvent> items)
        {
            //Recent Activities
            // -Any Case or SO where Created On<last 12 months
            // - Task, Email, Phone Call, Letter, SMS, Appointment where Created On < last 12 months
            // - Task, Email, Phone Call, Letter, SMS, where Due Date in the future(no limit)
            // - Appointment where Start Time in the future
            // - Customer Created On<last 12 month

            var ssnList = new List<string>();
            var now = DateTime.Now;
            
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i =>
                {
                    if (i.cap_lastactivitycreatedon_date != null)
                    {
                        DateTime lastactivityDate;
                        if (i.cap_lastactivitycreatedon_date.TryParseDate(out lastactivityDate))
                        {
                            if(lastactivityDate.HasRecentActivity())
                                ssnList.Add(item.SSN);
                        }
                        else { Console.WriteLine($"cap_lastactivitycreatedon_date ({i.cap_lastactivitycreatedon_date}) is not valid"); }
                    }
                   
                    if (i.cap_lastcasecreatedon_date != null) 
                    {
                        DateTime lastactivitycasecreate;
                        if (i.cap_lastcasecreatedon_date.TryParseDate(out lastactivitycasecreate))
                        {
                            if (lastactivitycasecreate.HasRecentActivity())
                                ssnList.Add(item.SSN);
                        }
                        else { Console.WriteLine($"cap_lastcasecreatedon_date ({i.cap_lastcasecreatedon_date}) is not valid"); }
                    }

                    if (i.cap_lastoptycreatedon_date != null)
                    { 
                        DateTime lastoptycreated;
                        if (i.cap_lastoptycreatedon_date.TryParseDate(out lastoptycreated))
                        {
                            if (lastoptycreated.HasRecentActivity())
                                ssnList.Add(item.SSN);
                        }
                        else { Console.WriteLine($"cap_lastoptycreatedon_date ({i.cap_lastoptycreatedon_date}) is not valid"); }
                    }

                    //SMS
                    var sms = GetSMS().Where(c => c.governmentid == i.governmentid).First();
                    if (sms!=null)
                    {
                        DateTime scheduledend;
                        if (sms.scheduledend.TryParseDate(out scheduledend))
                        {
                            if (scheduledend.HasRecentActivity())
                                ssnList.Add(item.SSN);
                        }
                        else { Console.WriteLine($"Scheduledend datetime ({sms.scheduledend}) is not valid"); }
                    }
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Check to see if a date is older than one year        
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>True if so</returns>
        public static bool HasRecentActivity(this DateTime dateTime)
        {
            var last = dateTime.AddYears(-1);
            var now = DateTime.Now;
            if (last < now)
                return true;
            return false;
        }


        /// <summary>
        /// Try parse a date to a valid DateTime 
        /// </summary>
        /// <param name="value"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static bool TryParseDate(this string value,  out DateTime dateTime)
        {

            //string format = "MM/dd/yyyy hh:mm:ss";
            //dateTime = DateTime.ParseExact(value, format, CultureInfo.InvariantCulture);
            //if (dateTime != null)
            //    return true;
            //else
            //    return false;

            string format = "yyyy-MM-dd HH:mm:ss";
            var time = Convert.ToDateTime(value);
            DateTime dt = DateTime.ParseExact(time.ToString(), format, CultureInfo.InvariantCulture);
            string formattedDate = dt.ToString(format);
            dateTime = Convert.ToDateTime(formattedDate);

            if (dateTime != null)
                return true;
            return false;
        }



        /// <summary>
        /// Not impl.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> SourceMigrating(this List<KundeRegisterEvent> items)
        {
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                    if (i.territorycode == MIGRATE)
                        ssnList.Add(item.SSN);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// End if customer is archieved...
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> Archieved(this List<KundeRegisterEvent> items)
        {
            // CRM Customer  is Inactive(Archieved)?
            //statuscode

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                    if (i.territorycode.Trim() == ARCHIVE)
                        ssnList.Add(item.SSN);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// End if customer is archieved...
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> Inactive(this List<KundeRegisterEvent> items)
        {
            // CRM Customer  is Inactive(Archieved)?
            //statuscode

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                    if (i.statuscode.Trim() == "1")
                        ssnList.Add(item.SSN);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Dont think I need this function due to function: HasOtherSources!! -----------------------------------------------------<
        /// #4 
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> OtherActiveSources(this List<KundeRegisterEvent> items)
        {
            var otherActiveSourcesList = new List<string>();
            var customers = items.GetSSN();

            return otherActiveSourcesList;
        }


        public static List<string> CustomerInCRMDsfBreeg(this List<KundeRegisterEvent> items)
        {
            //BankId(CRM) = BankId(KR)
            //OrgNum / FødNum(CRM) = OrgNum / FødNum(KR)
            //Source = Migraring
            //Status = Active or Inactive

            var bu = items.GetBusinessUnits();
            var customers = items.GetSSN();

            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                    if(item.Source.Equals(BREEG))
                        if (i.owningbusinessunit == item.BankNo)
                            ssnList.Add(item.SSN);
                });
            }
            return ssnList;
        }


        /// <summary>
        /// Compare bank in CRM to Bank in KR
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of SSN of items passed</returns>
        public static List<string> BankEqualsBank(this List<KundeRegisterEvent> items)
        {
            //OwnerBankNumber
            var equalList = new List<string>();
            var customers = items.GetSSN();
            var ssnList = new List<string>();
            foreach (var item in items)
            {
                CRMContacts(item.SSN)
                .ForEach(i => {
                        if (i.owningbusinessunit == item.BankNo)
                            ssnList.Add(item.SSN);
                });
            }
            return ssnList;
        }




        /// <summary>
        /// Compare bank in CRM to Bank in KR
        /// </summary>
        /// <param name="items"></param>
        /// <returns>A list of SSN of items passed</returns>
        public static List<string> BankCrmEqualsBankKr(this List<KundeRegisterEvent> items)
        {
            //OwnerBankNumber
            var equalList = new List<string>();
            //BankId(CRM) = BankId(KR)
            var customers = items.GetSSN();
            foreach (var ssn in customers)
            {
                var custInfo = CRMContacts(ssn);

                foreach (var cust in custInfo)
                {
                 
                }
            }

            return equalList;
        }


        public static List<string> SSNEqualsToKr(this List<KundeRegisterEvent> items)
        {
            var equalList = new List<string>();
            var customers = items.GetSSN();
            var bu = items.GetBusinessUnits();
            //OrgNum/FødNum(CRM) = OrgNum/FødNum (KR)

            foreach (var customer in items)
            {
                switch (customer.Source)
                {
                    case "CRM":
                        if (bu.Contains(customer.BankNo))
                            equalList.Add(customer.SSN);
                        break;
                    case "KERNE":
                        if (bu.Contains(customer.BankNo))
                            equalList.Add(customer.SSN);
                        break;
                    case "TRADEX":
                        if (bu.Contains(customer.BankNo))
                            equalList.Add(customer.SSN);
                        break;
                    case "NICE":
                        if (bu.Contains(customer.BankNo))
                            equalList.Add(customer.SSN);
                        break;
                    case "BANQSOFT":
                        if (bu.Contains(customer.BankNo))
                            equalList.Add(customer.SSN);
                        break;
                }                
            }

            return equalList;
        }



        public static List<string> BliKundeStatus(List<KundeRegisterEvent> items)
        {
            var list = new List<string>();

            return list;
        }


        /// <summary>
        /// Compares the state of items in list
        /// </summary>
        /// <param name="items"></param>
        /// <param name="status">Status to match</param>
        /// <returns>A list of valid SSN</returns>
        public static List<string> WhereActivityTypeIs(this List<KundeRegisterEvent> items, ActivityStatus status)
        {
            var list = new List<string>();
            foreach (var item in items)
            {
                if (item.Status == status)
                    list.Add(item.SSN);
            }
            return list;
        }


        /// <summary>
        /// The status of an Event.
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static List<string> CheckCustomerStatus(this List<KundeRegisterEvent> items)
        {
            var list = new List<string>();

            foreach(var item in items)
            {
                var statusToCompare = item.Status;
                switch (statusToCompare)
                {
                    case ActivityStatus.AKTIV:
                        case ActivityStatus.AKTIV_RELASJON:
                        case ActivityStatus.AVVENTER_SJEKK:
                        list.Add(item.SSN);
                        break;
                    case ActivityStatus.IKKE_FUNNET_AKTIV_I_KERNE:
                        list.Add(item.SSN);
                        break;
                    case ActivityStatus.KERNEID_IKKE_GYLDIG:
                        list.Add(item.SSN);
                        break;
                    case ActivityStatus.OFFENTLIGID_IKKE_I_KERNE:
                        case ActivityStatus.BANKREGNR_IKKE_EIKABANK:
                    case ActivityStatus.FIKTIVID_IKKE_I_KERNE:
                        list.Add(item.SSN);
                        break;
                }
            }

            return list;
        }
    }
}
