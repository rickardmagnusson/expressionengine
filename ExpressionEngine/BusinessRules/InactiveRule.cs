﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public class InactiveRule<T> : CompositeRule<T>
    {
        private List<string> targets;

        public InactiveRule(List<T> source, List<string> target) : base(source)
        {
            targets = target;
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent))
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");
            return targets.Contains(item.SSN);
        }
    }
}
