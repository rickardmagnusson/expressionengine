﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public class CheckStatusByGroupRule<T> : CompositeRule<T>
    {
        List<ActivityStatus> targets = new List<ActivityStatus> {
            ActivityStatus.AKTIV,
            ActivityStatus.AKTIV_RELASJON,
            ActivityStatus.AVVENTER_SJEKK };

  

        public CheckStatusByGroupRule(List<T> source) : base(source)
        {
        }

        //Businessrule
        public override bool IsSatisfiedBy(T o)
        {
            dynamic item;
            if (typeof(T) == typeof(KundeRegisterEvent)) //Maybe make this more generic
                item = (o as KundeRegisterEvent);
            else
                throw new Exception("Type is not valid");

            if (targets.Contains(item.Status))
                return true;
            return false;
        }
    }
}
