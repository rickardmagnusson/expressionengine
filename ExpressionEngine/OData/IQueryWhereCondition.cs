﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public interface IQueryWhereCondition : IQueryCondition
    {
        IQueryOrCondition EqualsTo(string compare);
    }
}
