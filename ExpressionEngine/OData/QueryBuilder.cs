﻿
using Microsoft.AspNet.OData.Builder;
using Microsoft.OData.Edm;
using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using API = Crm.Services.Extensions.OrganizationServiceHelper;

namespace ExpressionEngine.CRM
{
    /// <summary>
    /// Generates Odata queries.
    /// </summary>
    public class QueryBuilder : IQueryCondition, IQueryWhereCondition, IQueryOrCondition
    {
        /// <summary>
        /// Creates a new instance of QueryBuilder.
        /// </summary>
        /// <param name="entityName">Entity name pluralized (s)</param>
        private QueryBuilder(string entityName)
        {
            //HttpConfiguration conf = new HttpConfiguration {  };
            //var q = GetEdmModel(conf);

            entity = entityName;
        }


        public IEdmModel GetEdmModel(HttpConfiguration serviceProvider)
        {
            HttpClient client = API.WebApi();

            var builder = new ODataConventionModelBuilder();


            builder.EntitySet<CRMContact>("contacts")
                            .EntityType
                            .Filter() // Allow for the $filter Command
                            .Count() // Allow for the $count Command
                            .Expand() // Allow for the $expand Command
                            .OrderBy() // Allow for the $orderby Command
                            .Page() // Allow for the $top and $skip Commands
                            .Select();// Allow for the $select Command; 

            builder.EntitySet<BusinessUnit>("businessunits")
                            .EntityType
                            .Filter() // Allow for the $filter Command
                            .Count() // Allow for the $count Command
                            .Expand() // Allow for the $expand Command
                            .OrderBy() // Allow for the $orderby Command
                            .Page() // Allow for the $top and $skip Commands
                            .Select() // Allow for the $select Command
                            //.ContainsMany(x => x.)
                            .Expand();

            return builder.GetEdmModel();
        }


        /// <summary>
        /// Add an Entity name to query. (Pluralized, ends with an s)
        /// </summary>
        /// <param name="entityName">Entity name pluralized (s)</param>
        public static IQueryCondition AddEntity(string entityName)
        {
            query.Clear();
            queryfield.Clear();
            wherequery.Clear();
            return new QueryBuilder(entityName);
        }


        /// <summary>
        /// Add a field to query.
        /// </summary>
        /// <param name="fieldName">Field to add</param>
        /// <returns></returns>
        public IQueryCondition AddField(string fieldName)
        {
            if (string.IsNullOrEmpty(query.ToString()))
                query.Append("?$select=");
            
            queryfield.Append(fieldName + ",");
            return this;
        }


        /// <summary>
        /// Add a range of fields to query. 
        /// </summary>
        /// <param name="fieldNames">List of fields to add to query</param>
        /// <returns></returns>
        public IQueryCondition AddRange(string[] fieldNames)
        {
            var fields = string.Join(", ", fieldNames);
            queryfield.Append(fields);
            return this;
        }


        /// <summary>
        /// Adds a where statement to query. Must proceed with EqualsTo(condition)
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns>IQueryWhereCondition</returns>
        public IQueryWhereCondition Where(string fieldName)
        {
            if(string.IsNullOrEmpty(wherequery.ToString()))
                wherequery.Append($"$filter=");

            wherequery.Append($"{fieldName}");
            return this;
        }


        /// <summary>
        /// Add a condition parameter to compare from where.
        /// </summary>
        /// <param name="compare">The string to compare</param>
        /// <returns>IQueryOrCondition</returns>
        public IQueryOrCondition EqualsTo(string compare)
        {
            wherequery.Append($" eq '{compare}' ");
            return this;
        }


        /// <summary>
        /// Or not implemented yet
        /// </summary>
        /// <param name="s"></param>
        /// <returns>IQueryOrCondition</returns>
        [NotImplemented(message: "Not implemented yet.")]
        public IQueryOrCondition Or(string conditionOne)
        {
            orquery.Append($" '{conditionOne}' ");
            return this;
        }


        /// <summary>
        /// And not implemented yet
        /// </summary>
        /// <param name="s"></param>
        /// <returns>IQueryWhereCondition</returns>
        [NotImplemented(message: "Not implemented yet.")]
        public IQueryWhereCondition And(string conditionOne)
        {
            andquery.Append($" and {conditionOne}");
            return this;
        }


        public IQueryCondition Expand(string field, string what)
        {
            expandquery.Append($"/{field}({what})?$select=governmentid&$expand=Contact_Faxes($select=scheduledend)");
            return this;
        }


        /// <summary>
        /// Builds the query
        /// </summary>
        /// <returns></returns>
        public QueryBuilder Build()
        {
            return this;
        }


        private static string entity = string.Empty;
        private static StringBuilder query = new StringBuilder();
        private static StringBuilder queryfield = new StringBuilder();
        private static StringBuilder wherequery = new StringBuilder();
        private static StringBuilder andquery = new StringBuilder();
        private static StringBuilder orquery = new StringBuilder();
        private static StringBuilder expandquery = new StringBuilder();


        /// <summary>
        /// Constructs the query
        /// </summary>
        /// <returns>A OData query</returns>
        public override string ToString()
        {
            string addquery = string.IsNullOrEmpty(query.ToString()) ? "" : query.ToString();
            string addFields = string.IsNullOrEmpty(queryfield.ToString()) ? "" : queryfield.ToString().TrimEnd(',');
            string addwhere = string.IsNullOrEmpty(wherequery.ToString()) ? "" : "&" + wherequery.ToString();
            string andwhere = string.IsNullOrEmpty(andquery.ToString()) ? "" : "&" + andquery.ToString();
            string orwhere = string.IsNullOrEmpty(orquery.ToString()) ? "" : "&" + orquery.ToString();
            string expquery = string.IsNullOrEmpty(expandquery.ToString()) ? "" : "&" + expandquery.ToString();
          

            return entity +
                   addquery +
                   addFields +
                   addwhere + 
                   andwhere + 
                   orquery+ 
                   expquery;
        }
    }
}
