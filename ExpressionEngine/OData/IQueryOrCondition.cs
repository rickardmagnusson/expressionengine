﻿using ExpressionEngine.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public interface IQueryOrCondition : IQueryCondition
    {
        IQueryOrCondition Or(string compare);
    }
}
