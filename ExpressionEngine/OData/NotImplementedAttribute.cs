﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public class NotImplementedAttribute : Attribute
    {
        public string Message { get; set; }
        public NotImplementedAttribute(string message)
        {
            Message = message;
            throw new NotImplementedException(message);
        }
    }
}
