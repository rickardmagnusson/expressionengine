﻿using ExpressionEngine.CRM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    /// <summary>
    /// Interfaces for CRMQuery
    /// </summary>
    public interface IQueryCondition
    {
        IQueryWhereCondition And(string q);
        IQueryOrCondition Or(string q);
        IQueryWhereCondition Where(string fieldName);
        IQueryCondition AddField(string fieldName);
        IQueryCondition AddRange(string[] fields);

        QueryBuilder Build();
    }
}
