﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public enum ActivityStatus
    {
        Undefined,
        AKTIV_RELASJON,             //#1
        AKTIV,                      //#1
        AVVENTER_SJEKK,             //#1
        OFFENTLIGID_IKKE_I_KERNE,   //#2
        KERNEID_IKKE_GYLDIG,        //#3
        IKKE_FUNNET_AKTIV_I_KERNE,  //#4
        BANKREGNR_IKKE_EIKABANK,    //IGNORE
        FIKTIVID_IKKE_I_KERNE,      //IGNORE
    }
}
