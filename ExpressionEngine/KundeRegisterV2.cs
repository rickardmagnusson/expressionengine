﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ExpressionEngine.CRM;

namespace ExpressionEngine
{
    public class KundeRegisterV2
    {
        private static List<string> validSourceList = new List<string> { "KERNE" };
        private static List<string> validBankList = new List<string> { "4730" };
        Dictionary<string, CompositeRule<ExpressionEngine.KundeRegisterEvent>> rules; //Future


        /// <summary>
        /// Run thru all KundeRegisterEvent Rules
        /// </summary>
        /// <param name="list"></param>
        public void ProcessEvents(List<KundeRegisterEvent> list)
        {

            //##################### PREDEFINE RULES #################


            //SourceId = SourceId (Kerne) && Source = Kerne
            var validateSource = new SourceRule<KundeRegisterEvent>(list, validSourceList);

            //Bank = Bank, need help here to what will apply here!!
            var validateBanks = new ValidBanksRule<KundeRegisterEvent>(list, validBankList); //Need som more info here?

            //Status (statecode) Whats the customer Status
            var validateStatus = new CustomerStatusRule<KundeRegisterEvent>(list, list.GetSSN());

            //ActivityStatus enum from Event, on how where to proceed #1 or #9
            var validateActivity = new ActivityStatusRule<KundeRegisterEvent>(list, RuleHelper.ActivityStatusFields());

            //Has Other Sources
            var validateHasOtherSources = new OtherSourcesRule<KundeRegisterEvent>(list, list.HasOtherSources());

            //Has recent activities
            var validateRecentActivities = new RecentActivityRule<KundeRegisterEvent>(list, list.RecentActivities());

            // Group by Next function to call (Need to check when having two or more equal SSN) These goes to #1
            var validateActiveOrRelasjonOrAvventer = new CheckStatusByGroupRule<KundeRegisterEvent>(list);

            //Is source Migrate
            var validateSourceIsMigrate = new MigrateRule<KundeRegisterEvent>(list, list.SourceMigrating());

            //BankId(CRM) = BankId(KR)
            var bankCrmEqualsBankKr = new ValidateBankExistInKrRule<KundeRegisterEvent>(list, list.BankCrmEqualsBankKr());

            //OrgNum/FødNum(CRM) = OrgNum/FødNum (KR)
            var validateSSNEqualsToKr = new ValidCustomerSSNRule<KundeRegisterEvent>(list, list.SSNEqualsToKr());

            //---> #3 BANKREGNR_IKKE_EIKABANK, Deside where to go -->
            var coreIdNotValid = new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(ActivityStatus.BANKREGNR_IKKE_EIKABANK));

            //#4 IKKE_FUNNET_AKTIV_I_KERNE, Deside where to go -->
            var activeCoreIdNotFound = new CustomerStatusRule<KundeRegisterEvent>(list, list.WhereActivityTypeIs(ActivityStatus.IKKE_FUNNET_AKTIV_I_KERNE));

            //Id ActivityStatus is BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE
            var itemsToIgnore = new IgnoreRule<KundeRegisterEvent>(list); // If item flag is BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE

            //Is customer/account in archive
            var inArchive = new ArchievedRule<KundeRegisterEvent>(list, list.Archieved());

            //Is customer/account inactive
            var inActive = new InactiveRule<KundeRegisterEvent>(list, list.Inactive());



            //################ Grouped rules to achieve states #################


            var ignored = itemsToIgnore;
            var fail = ignored.PassedItems; //Have a status of BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE ? These items will be ignored on next step
            //Just ignore these items
            KundeRegisterEvent.AddToIgnore(fail);
            //Opposit statement here
            var pass = ignored.NotPassedItems; //Continue to #0



            //#0
            var zero =
                validateSource
                .And(validateBanks)
                .And(validateStatus);

            //Do something with items or continue
            pass = zero.PassedItems;    // Yes? ---> Check customer status
            fail = zero.NotPassedItems; // No? These items continues to #9 (Not found)


            //#1, AKTIV_RELASJON, AKTIV or AVVENTER_SJEKK
            pass.ForEach(c=> { Console.WriteLine($"{c.Id} goes to #1"); });
            fail.ForEach(c => { Console.WriteLine($"{c.Id} goes to #9"); });


            //#0.1
            var nine = bankCrmEqualsBankKr
                .And(validateSSNEqualsToKr)
                .And(validateStatus);

            //Do something with items or continue
            nine.PassedItems.ForEach(o => Console.WriteLine($"Passed rule 1.1: {o.Id} {o.Source}")); // ---> Found
            nine.NotPassedItems.ForEach(o => Console.WriteLine($"Fail rule 1.1: {o.Id} {o.Source}")); // ---> Not found


            var crmCustomerIsActive =
                 validateHasOtherSources
                    .And(validateSource)
                    .And(validateBanks)
                    .And(validateStatus)
                    .And(validateActiveOrRelasjonOrAvventer)
                    .And(validateRecentActivities);

            pass = crmCustomerIsActive.PassedItems;
            fail = crmCustomerIsActive.NotPassedItems;

            Console.WriteLine("\nCustomer is Active ------------>");
            pass.ForEach(o => Console.WriteLine($"Has other source and has recent activities : {o.Id} {o.Source}"));
            Console.WriteLine("Customer not Active ------------>");
            fail.ForEach(o => Console.WriteLine($"Failed rules: {o.Id} {o.Source}"));


            //#2 --->
            var two = validateHasOtherSources;

            pass = two.PassedItems;
            fail = two.NotPassedItems;

            var recent = validateRecentActivities;


            //#3 --->
            var three = coreIdNotValid.PassedItems;
            //Ugyldig Offentlig ID = Yes
            Console.WriteLine("\nKERNEID_IKKE_GYLDIG  ------------>");
            three.ForEach(o => { UpdateAccountPublicIdInvalid(o); Console.WriteLine($"Update Customer with all information provided from the event. In addtion set: { o.Id} {o.Source}"); });
            //Add those to ignore since their done.
            KundeRegisterEvent.AddToIgnore(three);


            //#4 ---> Customer has other Active Sources?*
            pass = validateHasOtherSources.PassedItems;
            fail = validateHasOtherSources.NotPassedItems;
            Console.WriteLine("\nKERNEID_IKKE_GYLDIG  ------------>");
            pass.ForEach(o => { Updateaccount(o, false); Console.WriteLine($"Update only customer personal information with the data from the event!: { o.Id} {o.Source}");  });
            fail.ForEach(o => { Updateaccount(o, true); Console.WriteLine($"Update Customer with all information provided from the event. Dont update source and type: { o.Id} {o.Source}"); });



            //#9 --->
            Console.WriteLine("\nFoundCustomer ------------>");
            pass.ForEach(o => Console.WriteLine($"Passed rule Source: {o.Id} {o.Source}"));
            Console.WriteLine("FoundCustomer failed ------------>");
            fail.ForEach(o => Console.WriteLine($"Not passed rule Source. Reason: Source is {o.Source} on Customer ({o.Id})"));

            
            pass = itemsToIgnore.PassedItems;
            Console.WriteLine("\nCustomers to Ignore ------------>");
            pass.ForEach(o => Console.WriteLine($"Ignored due to: {o.Id} {o.Source} has BANKREGNR_IKKE_EIKABANK or FIKTIVID_IKKE_I_KERNE")); //#End (found items to Ignore)

            var customerStatus = validateActiveOrRelasjonOrAvventer;
   

            var passCustomerStatus = customerStatus.PassedItems;
            var failCustomerStatus = customerStatus.NotPassedItems;

            Console.WriteLine("\ncustomerStatus ------------>");
            passCustomerStatus.ForEach(o => Console.WriteLine($"validateActiveOrRelasjonOrAvventer true: {o.Id} {o.Source}"));
            Console.WriteLine("customerStatus fail on ------------>");
            failCustomerStatus.ForEach(o => Console.WriteLine($"validateActiveOrRelasjonOrAvventer false: {o.Id} {o.Source}"));

    


            #region FUTURE


            //################ FUTURE: Grouped rules to achieve states thru a Dictionary ###################

            //rules = new Dictionary<string, CompositeRule<KundeRegisterEvent>>();
            //rules.Add(nameof(validateSource), validateSource);
            //rules.Add(nameof(validateBanks), validateBanks);
            //rules.Add(nameof(validateStatus), validateStatus);
            //rules.Add(nameof(validateActivity), validateActivity);
            //rules.Add(nameof(validateSourceIsMigrate), validateSourceIsMigrate);
            //rules.Add(nameof(validateHasOtherSources), validateHasOtherSources);
            //rules.Add(nameof(validateRecentActivities), validateRecentActivities);
            //rules.Add(nameof(validateExistAsArchiveOrInactive), validateExistAsArchiveOrInactive);
            //rules.Add(nameof(validateActiveOrRelasjonOrAvventer), validateActiveOrRelasjonOrAvventer);

            //Future. Create Grouping with a fluent Dictionary for more clarity of Rules
            //From dictionary
            //var Source = Rule(nameof(validateSource))
            //            .And(Rule(nameof(validateBanks)));

            //IReadOnlyDictionary<string, string> CheckActivity =  new Dictionary<string, string>()
            //    .AddValueWithKeys(nameof(validateSource), nameof(validateActivity), nameof(validateStatus));

            //################################ FUTURE: // ###################################


            #endregion
        }


        public void Updateaccount(KundeRegisterEvent account, bool keepSourceAndStatus)
        {

        }

        public void UpdateAccountPublicIdInvalid(KundeRegisterEvent account)
        {

        }


        //Future impl.
        public CompositeRule<KundeRegisterEvent> Rule(string ruleName)
        {
            if (rules.ContainsKey(ruleName)) {
                CompositeRule<KundeRegisterEvent> rule = null;
                rules.TryGetValue(ruleName, out rule);
                return rule;
            }
            return null;
        }


        //Only for test of fields
        public static void GetField()
        {
            //var val = new ExpressionEngine.GenModel.Microsoft.Dynamics.CRM.Contact().Statuscode.Value;
        }
    }


    #region FUTURE


    //##################################### FUTURE CLASSES ####################################

    /// <summary>
    /// Future class where to group rules and pass the rules thru one argument.
    /// </summary>
    public static class DictionaryHelper
    {
        public static Dictionary<TKey, TValue> AddValueWithKeys<TKey, TValue>(
            this Dictionary<TKey, TValue> dictionary,
            TValue value,
            params TKey[] keys)
        {
            foreach (var key in keys) dictionary.Add(key, value);
            return dictionary;
        }


        public static CompositeRule<KundeRegisterEvent> Rule(this string ruleName, Dictionary<string, CompositeRule<KundeRegisterEvent>> rules)
        {
            if (rules.ContainsKey(ruleName))
            {
                CompositeRule<KundeRegisterEvent> rule = null;
                rules.TryGetValue(ruleName, out rule);
                return rule;
            }
            return null;
        }

        public static void Execute(Dictionary<string, string> rules)
        {
            foreach(var rule in rules.Keys)
            {
                string rl;
                var r = rules.TryGetValue(rule, out rl);
            }
        }
    }

    //https://codereview.stackexchange.com/questions/153877/methods-to-create-c-dictionaries-with-static-keys-values-in-fluent-notation
    public class FluentDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        private readonly Dictionary<TKey, TValue> _dictionary;

        public ICollection<TKey> Keys => _dictionary.Keys;
        public ICollection<TValue> Values => _dictionary.Values;

        public int Count => _dictionary.Count;
        public bool IsReadOnly => false;

        ICollection<TKey> IDictionary<TKey, TValue>.Keys => throw new NotImplementedException();

        ICollection<TValue> IDictionary<TKey, TValue>.Values => throw new NotImplementedException();

        int ICollection<KeyValuePair<TKey, TValue>>.Count => throw new NotImplementedException();

        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly => throw new NotImplementedException();

        TValue IDictionary<TKey, TValue>.this[TKey key] { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public TValue this[TKey key]
        {
            get { return _dictionary[key]; }
            set { _dictionary[key] = value; }
        }

        public FluentDictionary()
        {
            _dictionary = new Dictionary<TKey, TValue>();
        }

        public FluentDictionary(Dictionary<TKey, TValue> source)
        {
            _dictionary = source;
        }

        public FluentDictionary(IEnumerable<KeyValuePair<TKey, TValue>> source)
        {
            _dictionary = source.ToDictionary(pair => pair.Key, pair => pair.Value);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() => _dictionary.GetEnumerator();

        public void Add(TKey key, TValue value) => _dictionary.Add(key, value);

        public void Add(KeyValuePair<TKey, TValue> item) => Add(item.Key, item.Value);

        public void Add(TKey[] keys, TValue value)
        {
            foreach (var key in keys)
            {
                Add(key, value);
            }
        }

        public void Clear() => _dictionary.Clear();

        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            TValue value;
            return _dictionary.TryGetValue(item.Key, out value) &&
                    EqualityComparer<TValue>.Default.Equals(value, item.Value);
        }

        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            ICollection<KeyValuePair<TKey, TValue>> collection = new List<KeyValuePair<TKey, TValue>>(_dictionary);
            collection.CopyTo(array, arrayIndex);
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            return this.Contains(item) && Remove(item.Key);
        }

        public bool ContainsKey(TKey key) => _dictionary.ContainsKey(key);

        public bool Remove(TKey key) => _dictionary.Remove(key);

        public bool TryGetValue(TKey key, out TValue value) => _dictionary.TryGetValue(key, out value);

        bool IDictionary<TKey, TValue>.ContainsKey(TKey key)
        {
            throw new NotImplementedException();
        }

        void IDictionary<TKey, TValue>.Add(TKey key, TValue value)
        {
            throw new NotImplementedException();
        }

        bool IDictionary<TKey, TValue>.Remove(TKey key)
        {
            throw new NotImplementedException();
        }

        bool IDictionary<TKey, TValue>.TryGetValue(TKey key, out TValue value)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            throw new NotImplementedException();
        }

        void ICollection<KeyValuePair<TKey, TValue>>.Clear()
        {
            throw new NotImplementedException();
        }

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            throw new NotImplementedException();
        }

    }

    #endregion
}
