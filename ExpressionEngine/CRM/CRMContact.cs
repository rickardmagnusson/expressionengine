﻿using System.ComponentModel.DataAnnotations;

namespace ExpressionEngine.CRM
{
    /// <summary>
    /// Same as CrmCustomer in Common project
    /// </summary>
    public class CRMContact
    {

        public string cap_banqsoftsourceid { get; set; }
        public string cap_nice2sourceid { get; set; }
        public string cap_nicesourceid { get; set; }
        public string cap_sourceid { get; set; }
        public string customertypecode { get; set; }
        public string mobilephone { get; set; }
        public string birthdate { get; set; }
        public string merged { get; set; }
        public string gendercode { get; set; }
        public string cap_lastactivitycreatedon_state { get; set; }
        public string cap_lastoptycreatedon { get; set; }
        public string emailaddress1 { get; set; }
        public string haschildrencode { get; set; }
        public string preferredappointmenttimecode { get; set; }
        public string isbackofficecustomer { get; set; }
        public string modifiedon { get; set; }
        public string msdyn_orgchangestatus { get; set; }
        public string address1_composite { get; set; }
        public string lastname { get; set; }
        public string donotpostalmail { get; set; }
        public string marketingonly { get; set; }
        public string donotphone { get; set; }
        public string preferredcontactmethodcode { get; set; }
        public string educationcode { get; set; }
        public string ownerid { get; set; }
        public string customersizecode { get; set; }
        public string firstname { get; set; }
        public string yomifullname { get; set; }
        public string address2_addresstypecode { get; set; }
        public string address1_line1 { get; set; }
        public string donotemail { get; set; }
        public string address2_shippingmethodcode { get; set; }
        public string fullname { get; set; }
        public string timezoneruleversionnumber { get; set; }
        public string address1_addressid { get; set; }
        public string msdyn_gdproptout { get; set; }
        public string address2_freighttermscode { get; set; }
        public string statuscode { get; set; }
        public string createdon { get; set; }
        public string address1_stateorprovince { get; set; }
        public string donotsendmm { get; set; }
        public string donotfax { get; set; }
        public string leadsourcecode { get; set; }
        public string cap_lastcasecreatedon_state { get; set; }
        public string address1_country { get; set; }
        public string cap_lastactivitycreatedon_date { get; set; }
        public string address1_line2 { get; set; }
        public string address1_line3 { get; set; }
        public string creditonhold { get; set; }
        public string telephone1 { get; set; }
        public string cap_lastoptycreatedon_date { get; set; }
        public string address3_addressid { get; set; }
        public string donotbulkemail { get; set; }
        public string cap_lastoptycreatedon_state { get; set; }
        public string modifiedby { get; set; }
        public string followemail { get; set; }
        public string shippingmethodcode { get; set; }
        public string createdby { get; set; }
        public string address1_city { get; set; }
        public string donotbulkpostalmail { get; set; }
        public string territorycode { get; set; }
        public string cap_lastcasecreatedon_date { get; set; }
        public string owningteam { get; set; }
        public string cap_tradexsourceid { get; set; }
        public string contactid { get; set; }
        public string cap_eventdate { get; set; }
        public string participatesinworkflow { get; set; }
        public string statecode { get; set; }
        public string owningbusinessunit { get; set; }
        public string address2_addressid { get; set; }
        public string address1_postalcode { get; set; }
        public string governmentid { get; set; }

    }
}
