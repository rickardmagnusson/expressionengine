﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine.CRM
{
    /// <summary>
    /// Load all instances of queries to database
    /// </summary>
    public class CRMCache
    {
        static CRMCache _instance;
        private CRMCache() { }


        /// <summary>
        /// Creates a new instance of CRMCache.
        /// </summary>
        /// <returns>CRMCache instance</returns>
        public static CRMCache Instance()
        {
            if (_instance == null)
            {
                var list = KundeRegisterEvent.Create();
                _instance = new CRMCache();
                _instance.Contacts = RuleHelper.LoadContacts(list);
                _instance.SMS = RuleHelper.LoadSMS(list);
                _instance.BusinessUnits = RuleHelper.LoadBusinessUnits(list);
            }
                
            return _instance;
        }

        /// <summary>
        /// A list of BusinessUnits from cache
        /// </summary>
        public List<BusinessUnit> BusinessUnits { get; private set; }

        /// <summary>
        /// A list of Contacts from cache
        /// </summary>
        public List<CRMContact> Contacts { get; private set; }

        public List<SMS> SMS { get; private set; }
    }
}
