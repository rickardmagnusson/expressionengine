﻿using ExpressionEngine.CRM;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ExpressionEngine
{

    class Program
    {
        static void Main(string[] args) 
        {

            var list = KundeRegisterEvent.Create();
            //Hook onto KundeRegister process.
            new KundeRegisterV2().ProcessEvents(list);
            
            Console.ReadLine();
        }
    }
}
