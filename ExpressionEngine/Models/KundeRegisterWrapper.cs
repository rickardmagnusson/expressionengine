﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpressionEngine
{
    public class KundeRegisterEvent
    {

        private static List<KundeRegisterEvent> EventList = new List<KundeRegisterEvent>
        {
            new KundeRegisterEvent { Id = 1, Source = "KERNE",  BankNo = "4730", SSN = "11127033767", SourceId="4751", Status = ActivityStatus.AKTIV  }, //Pass
            new KundeRegisterEvent { Id = 2, Source = "KERNE",  BankNo = "4730", SSN = "18034023472", SourceId="3713", Status = ActivityStatus.AKTIV_RELASJON}, //Pass
            new KundeRegisterEvent { Id = 3, Source = "KERNE",  BankNo = "4730", SSN = "30116030442", SourceId="5625", Status = ActivityStatus.AVVENTER_SJEKK}, //Pass
            new KundeRegisterEvent { Id = 4, Source = "TRADEX", BankNo = "4730", SSN = "01061548692", SourceId="3290", Status = ActivityStatus.AKTIV}, //Failed on source, because is TRADEX, will find you later
            new KundeRegisterEvent { Id = 5, Source = "KERNE",  BankNo = "4730", SSN = "01061548692", SourceId="1555", Status = ActivityStatus.OFFENTLIGID_IKKE_I_KERNE},  //Will fails since theres no SourceId in CRM
            new KundeRegisterEvent { Id = 6, Source = "KERNE",  BankNo = "4730", SSN = "10131983576", SourceId="1414", Status = ActivityStatus.BANKREGNR_IKKE_EIKABANK}, //test ignore
            new KundeRegisterEvent { Id = 7, Source = "KERNE",  BankNo = "4730", SSN = "10131983576", SourceId="1424", Status = ActivityStatus.KERNEID_IKKE_GYLDIG} //Ignore
        };


        public int Id { get; set; }
        public string Source { get; set; }
        public string BankNo { get; set; }

        public string SSN { get; set; }

        public string SourceId { get; set; }

        public ActivityStatus Status { get; set; }

        public static List<KundeRegisterEvent> Create()
        {
            return EventList;
        }


        public static void AddToIgnore(List<KundeRegisterEvent> items)
        {
            if(EventList.Any())
                items.ForEach(i=> EventList.Remove(i));
        }
    }

    //Helper class
    public class BusinessUnit
    {
        public string businessunitid { get; set; }
        public string divisionname { get; set; }
    }


    public class SMS
    {
        public string governmentid { get; set; }
        public string scheduledend { get; set; }
    }
}
